package mn.ailab;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

public class Http {
    public static String executePost(String targetURL, List<NameValuePair> params) {
        HttpURLConnection connection = null;
        try {
            HttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost(targetURL);
            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);
            System.out.println(response.toString());

            if(response.getStatusLine().getStatusCode() > 200){
                return "error";
            }else{
                return "success";
            }
        } catch (Exception e) {
            return "error";
        }
    }

    public static String executeTelegramPhotosPost(String targetURL, List<NameValuePair> params) {
        HttpURLConnection connection = null;
        try {
            HttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost(targetURL);
//            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            httppost.addHeader("accept","application/json");
            httppost.addHeader("User-Agent","Telegram Bot SDK - (https://github.com/irazasyed/telegram-bot-sdk)");
            httppost.addHeader("content-type","application/json");
            HttpResponse response = httpclient.execute(httppost);
            System.out.println(response.toString());
            if(response.getStatusLine().getStatusCode() > 200){
                return "error";
            }else{
                return "success";
            }
        } catch (Exception e) {
            return "error";
        }
    }
}
